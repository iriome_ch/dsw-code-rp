<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Iriome Cabrera Heredia</title>
</head>

<body>
    <h1>Welcome to my guessing game</h1>
    <p> <?php
        $numero =12354354;
        $guestnumber = htmlspecialchars($_GET["guest"]);

        if ($guestnumber === null) {
            Echo "Missing guess parameter";
        } elseif (!(is_numeric($guestnumber))) {
            Echo "Your guess is not a number";
        }elseif ($guestnumber > $numero) {
            Echo "Your guess is too high";
        }elseif ($guestnumber < $numero) {
            echo "Your guess is too low";
        } else {
           echo "Congratulations - You are right";
        }
        
        
        ?></p>

</body>

</html>